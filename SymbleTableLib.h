#ifndef SYMBLETABLELIB_H
#define SYMBLETABLELIB_H

// * DNT
#include "SymbleTable.h"

typedef struct SymbleTableLib {
    bool (*updateSymble)(SymbleTable s, uint Index, char *Name);
    void (*swapID)(SymbleTable s, int ID, int ID2);
    uint (*getSize)(SymbleTable s);
    uint (*getNumberOfSymbles)(SymbleTable s);
    void (*deleteSymbleByID)(SymbleTable s, int ID);
    void (*deleteSymble)(SymbleTable s, char *Name);
    SymbleTable (*new)(uint MaxItems);
    void (*free)(SymbleTable s);
    void (*clean)(SymbleTable s);
    uint (*addSymbleFromChar)(SymbleTable s, char Name);
    uint (*addSymble)(SymbleTable s, char *Name);
    Symble (*getSymbleFromChar)(SymbleTable s, char Name);
    Symble (*getSymbleByIndex)(SymbleTable s, uint Index);
    Symble (*getSymbleByID)(SymbleTable s, int ID);
    Symble (*getSymble)(SymbleTable s, char *Name);
    bool (*checkSymbleByID)(SymbleTable s, int ID);
    bool (*checkSymble)(SymbleTable s, char *Name);
    void (*changeIDByIndex)(SymbleTable s, int index, int id);
    void (*changeID)(SymbleTable s, char *Name, int id);
} SymbleTableLib;

// * EDNT

SymbleTableLib newSymbleTableLib();

#endif // ! SYMBLETABLELIB_H
