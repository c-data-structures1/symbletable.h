#include "SymbleTable.h"
#include "Array.h/Array.c"
#include "Array.h/CUtils.h/CUtils.c"
#include "Symble.h/Symble.c"

struct SymbleTable {
    Array Table;
    uint AddedItems;
};

// Get symble at index
Symble SymbleTable__getSymbleByIndex(SymbleTable s, uint Index) {
    return Array__get(s->Table, Index);
}

// * STATIC

// Return true if values it's out of range
static bool OutOfRange(SymbleTable s, uint Index) {
    return Index >= s->AddedItems;
}

// Return true if the max number of items has been reached
static bool MaxLimit(SymbleTable s) {
    return s->AddedItems >= s->Table->ItemsNumber;
}

// Resize ST
static void resizeTable(SymbleTable s, uint newSize) {
    Array__resize(s->Table, newSize, true);
}

// Double the size of the table
static void doubleTable(SymbleTable s) {
    resizeTable(s, s->Table->ItemsNumber * 2);
}

// Add a new Symble to the ST
static void addSymble(SymbleTable s, Symble i) {
    for (size_t j = 0; j < s->Table->ItemsNumber; j++) {
        if (Array__get(s->Table, j) == NULL) {
            Array__set(s->Table, j, i);
            break;
        }
    }
}

// Set symble at index
static void setSymble(SymbleTable s, uint Index, Symble i) {
    Array__set(s->Table, Index, i);
}

// Check if index it's not empty and in range
static bool checkSymbleByIndex(SymbleTable s, uint Index) {
    if (OutOfRange(s, Index)) { return false; }
    return SymbleTable__getSymbleByIndex(s, Index) != NULL;
}

// Swap index of two symbles inside the ST
static bool swapSymbleIndex(SymbleTable s, uint Index, uint OtherIndex) {
    if (OutOfRange(s, Index)) { return false; }
    if (OutOfRange(s, OtherIndex)) { return false; }

    Symble temp  = SymbleTable__getSymbleByIndex(s, Index);
    Symble temp2 = SymbleTable__getSymbleByIndex(s, OtherIndex);
    setSymble(s, Index, temp2);
    setSymble(s, OtherIndex, temp);

    return true;
}

/* Return symble index, -1 if symble doesn't exist
 */
static uint getSymbleIndex(SymbleTable s, char *Name) {
    for (uint i = 0; i < s->AddedItems; i++) { // Per ogni elemento inserito
        Symble tmp = SymbleTable__getSymbleByIndex(s, i);
        if (equalStrings(tmp->Name, Name)) { // Se l'elemento è uguale
            return i;
        }
    }
    return (uint)-1;
}

/* Return symble index, -1 if symble doesn't exist
 */
static uint getSymbleIndexByID(SymbleTable s, int ID) {
    Symble tmp = NULL;
    for (uint i = 0; i < s->AddedItems; i++) {
        tmp = SymbleTable__getSymbleByIndex(s, i);
        if (tmp != NULL) {
            if (tmp->ID == ID) { return i; }
        }
    }
    return (uint)-1;
}

// Return index of the last not-null Symble in the ST
static uint lastValidIndex(SymbleTable s) {
    for (size_t i = s->Table->ItemsNumber - 1; i >= 0; i--) {
        if (SymbleTable__getSymbleByIndex(s, i) != NULL) { return i; }
    }
    return 0;
}

// * END OF STATIC

// Check if a symble with the same name already exist
bool SymbleTable__checkSymble(SymbleTable s, char *Name) {
    uint index = getSymbleIndex(s, Name);
    return index == (uint)-1 ? false : true;
}

// Check if a symble with the same ID already exist
bool SymbleTable__checkSymbleByID(SymbleTable s, int ID) {
    uint index = getSymbleIndexByID(s, ID);
    return index == (uint)-1 ? false : true;
}

// Search and return symble, return NULL if not found
Symble SymbleTable__getSymble(SymbleTable s, char *Name) {
    uint index = getSymbleIndex(s, Name);
    return index == -1 ? NULL : SymbleTable__getSymbleByIndex(s, index);
}

// Search and return symble, return NULL if not found
Symble SymbleTable__getSymbleFromChar(SymbleTable s, char Name) {
    char *tmpName = fromCharToString(Name);
    uint index    = getSymbleIndex(s, tmpName);
    free(tmpName);
    return index == -1 ? NULL : SymbleTable__getSymbleByIndex(s, index);
}

// Search and return symble by ID, return NULL if not found
Symble SymbleTable__getSymbleByID(SymbleTable s, int ID) {
    uint index = getSymbleIndexByID(s, ID);
    return index == -1 ? NULL : SymbleTable__getSymbleByIndex(s, index);
}

/* Add a new symble with the specified name
 * return Symble ID
 */
uint SymbleTable__addSymble(SymbleTable s, char *Name) {
    if (MaxLimit(s)) { doubleTable(s); }
    uint index = getSymbleIndex(s, Name);
    if (index != -1) { return SymbleTable__getSymbleByIndex(s, index)->ID; }
    Symble tmp = Symble__new(strlen(Name));
    Symble__updateName(tmp, Name);
    addSymble(s, tmp);
    tmp->ID = s->AddedItems++;
    return tmp->ID;
}

/* Add a new symble with the specified name
 * return Symble ID
 */
uint SymbleTable__addSymbleFromChar(SymbleTable s, char Name) {
    char *tmp2Name = fromCharToString(Name);
    char tmpName[strlen(tmp2Name) + 1];
    strcpy(tmpName, tmp2Name);
    free(tmp2Name);

    if (MaxLimit(s)) { doubleTable(s); }
    uint index = getSymbleIndex(s, tmpName);
    if (index != -1) { return SymbleTable__getSymbleByIndex(s, index)->ID; }
    Symble tmp = Symble__new(strlen(tmpName));
    Symble__updateNameChar(tmp, Name);
    addSymble(s, tmp);
    tmp->ID = s->AddedItems++;

    return tmp->ID;
}

// Delete unused ST cells
void SymbleTable__clean(SymbleTable s) { Array__minimize(s->Table, false); }

// Free ST
void SymbleTable__free(SymbleTable s) {
    Array__free(s->Table, true);
    free(s);
}

// Allocate space for a new ST
SymbleTable SymbleTable__new(uint MaxItems) {
    SymbleTable s = malloc(sizeof(struct SymbleTable));
    s->Table      = Array__new((void *)&Symble__free, NULL);
    Array__allocate(s->Table, MaxItems);
    s->AddedItems = 0;
    return s;
}

// Delete a symble from the ST
void SymbleTable__deleteSymble(SymbleTable s, char *Name) {
    uint index = getSymbleIndex(s, Name);
    if (index == -1) { return; }
    uint lastValid = lastValidIndex(s);

    Array__swap(s->Table, index, lastValid);
    Symble__free(SymbleTable__getSymbleByIndex(s, lastValid));
    setSymble(s, lastValid, NULL);
    s->AddedItems--;
}

// Delete a symble from the ST
void SymbleTable__deleteSymbleByID(SymbleTable s, int ID) {
    uint index = getSymbleIndexByID(s, ID);
    if (index == -1) { return; }
    uint lastValid = lastValidIndex(s);

    Array__swap(s->Table, index, lastValid);
    Symble__free(SymbleTable__getSymbleByIndex(s, lastValid));
    setSymble(s, lastValid, NULL);
    s->AddedItems--;
}

// Return number of items inside the ST
uint SymbleTable__getNumberOfSymbles(SymbleTable s) { return s->AddedItems; }

// Return size of the ST
uint SymbleTable__getSize(SymbleTable s) { return s->Table->ItemsNumber; }

// Update symble name from ST
bool SymbleTable__updateSymble(SymbleTable s, uint Index, char *Name) {
    if (OutOfRange(s, Index)) { // Interruzione numero non possibile
        return false;
    }
    Symble temp = SymbleTable__getSymbleByIndex(s, Index);
    Symble__updateName(temp, Name);
    return true;
}

// Swap two symble's IDs
void SymbleTable__swapID(SymbleTable s, int id, int id2) {
    Symble tmp = SymbleTable__getSymbleByID(s, id);
    if (tmp == NULL) { return; }

    Symble tmp2 = SymbleTable__getSymbleByID(s, id2);
    if (tmp2 == NULL) { return; }

    tmp->ID  = id2;
    tmp2->ID = id;
}

// Change a symble ID
void SymbleTable__changeIDByIndex(SymbleTable s, int index, int id) {
    Symble tmp = SymbleTable__getSymbleByIndex(s, index);
    tmp->ID    = id;
}

// Change a symble ID
void SymbleTable__changeID(SymbleTable s, char *Name, int id) {
    int index = getSymbleIndex(s, Name);
    if (index != -1) { SymbleTable__getSymbleByIndex(s, index); }
}