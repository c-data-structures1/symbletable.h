#include "SymbleTableLib.h"
SymbleTableLib SymbleTables;

int main(int argc, char const *argv[]) {
    SymbleTables = newSymbleTableLib();
    int max      = 26;
    char start   = 'A';

    SymbleTable table = SymbleTables.new(max);

    for (size_t i = 0; i < max; i++) {
        SymbleTables.addSymbleFromChar(table, start);
        start += 1;
    }

    for (size_t i = 0; i < max; i++) {
        Symble tmp = SymbleTables.getSymbleByID(table, i);
        printf("Symble %d : %s\n", i, tmp->Name);
    }

    char test[] = "HELLO WORD";
    printf("\n%s\n", test);
    for (size_t i = 0; i < strlen(test); i++) {
        if (test[i] != ' ') {
            printf("%d;", SymbleTables.getSymbleFromChar(table, test[i])->ID);
        } else {
            printf(" ");
        }
    }
    printf("\n\n");

    SymbleTables.swapID(table, 0, 5);
    SymbleTables.swapID(table, 1, 5);
    SymbleTables.swapID(table, 2, 5);
    SymbleTables.swapID(table, 3, 8);
    SymbleTables.swapID(table, 4, 14);

    for (size_t i = 5; i < max; i++) {
        SymbleTables.deleteSymbleByID(table, i);
    }

    SymbleTables.clean(table);

    int numberOfItems = SymbleTables.getNumberOfSymbles(table);
    for (size_t i = 0; i < numberOfItems; i++) {
        Symble tmp = SymbleTables.getSymbleByID(table, i);
        printf("Symble %d : %s\n", i, tmp->Name);
    }

    SymbleTables.free(table);

    return 0;
}
