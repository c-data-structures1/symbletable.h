#include "Symble.h"

static char *fromCharToString(char c) {
    char *tmp = calloc(sizeof(*tmp), 2);
    tmp[0]    = c;
    tmp[1]    = '\0';
    return tmp;
}

// Resize name
static void fixNameSize(Symble s, char *NewName) {
    unsigned int Length = strlen(NewName);
    if (Length > s->__NameLength) {
        char temp[s->__NameLength];
        strcpy(s->Name, temp);
        free(s->Name);
        s->__NameLength = Length;
        s->Name         = calloc(s->__NameLength + 1, sizeof(char));
    }
}

// Free a symble
void Symble__free(Symble s) {
    if (s == NULL) { return; }
    free(s->Name);
    free(s);
}

// Allocate and return a new symble
Symble Symble__new(unsigned int NameLength) {
    Symble s        = malloc(sizeof(struct Symble));
    s->__NameLength = NameLength;
    s->Name         = calloc(s->__NameLength + 1, sizeof(char));
    return s;
}

// Update symble name
void Symble__updateName(Symble s, char *NewName) {
    fixNameSize(s, NewName);
    strcpy(s->Name, NewName);
}

void Symble__updateNameChar(Symble s, char NewName) {
    char *tmpName = fromCharToString(NewName);
    Symble__updateName(s, tmpName);
    free(tmpName);
}