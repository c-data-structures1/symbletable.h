#ifndef SYMBLE_H
#define SYMBLE_H

// * DNT
#include <stdlib.h>
#include <string.h>

typedef struct Symble {
    int ID;
    char *Name;
    unsigned int __NameLength;
} * Symble;
// * EDNT

void Symble__updateNameChar(Symble s, char NewName);
void Symble__updateName(Symble s, char *NewName);
Symble Symble__new(unsigned int NameLength);
void Symble__free(Symble s);

#endif // ! SYMBLE_H
