#include "SymbleTableLib.h"
#include "SymbleTable.c"

SymbleTableLib newSymbleTableLib() {
    SymbleTableLib l;

    l.addSymble          = SymbleTable__addSymble;
    l.addSymbleFromChar  = SymbleTable__addSymbleFromChar;
    l.checkSymble        = SymbleTable__checkSymble;
    l.checkSymbleByID    = SymbleTable__checkSymbleByID;
    l.clean              = SymbleTable__clean;
    l.deleteSymble       = SymbleTable__deleteSymble;
    l.deleteSymbleByID   = SymbleTable__deleteSymbleByID;
    l.free               = SymbleTable__free;
    l.getNumberOfSymbles = SymbleTable__getNumberOfSymbles;
    l.getSize            = SymbleTable__getSize;
    l.getSymble          = SymbleTable__getSymble;
    l.getSymbleByID      = SymbleTable__getSymbleByID;
    l.getSymbleByIndex   = SymbleTable__getSymbleByIndex;
    l.getSymbleFromChar  = SymbleTable__getSymbleFromChar;
    l.new                = SymbleTable__new;
    l.swapID             = SymbleTable__swapID;
    l.updateSymble       = SymbleTable__updateSymble;
    l.changeID           = SymbleTable__changeID;
    l.changeIDByIndex    = SymbleTable__changeIDByIndex;

    return l;
}